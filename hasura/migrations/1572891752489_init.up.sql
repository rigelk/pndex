CREATE FUNCTION public.set_current_timestamp_updated() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated" = NOW();
  RETURN _new;
END;
$$;
CREATE FUNCTION public.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;
CREATE TABLE public.categories (
    id integer NOT NULL,
    name text NOT NULL
);
COMMENT ON TABLE public.categories IS 'A table of video categories';
CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;
CREATE TABLE public.channels (
    id text NOT NULL,
    name text NOT NULL,
    url text NOT NULL,
    summary text NOT NULL,
    support text NOT NULL,
    icon jsonb NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    "isHidden" boolean DEFAULT false NOT NULL
);
COMMENT ON TABLE public.channels IS 'A table of video channel actors';
CREATE TABLE public.instances (
    id text NOT NULL,
    "isCollected" boolean DEFAULT true NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    "isHidden" boolean DEFAULT false NOT NULL,
    name text,
    nodeinfo jsonb,
    last_activity_fetched text,
    "isOffline" boolean DEFAULT false NOT NULL
);
COMMENT ON TABLE public.instances IS 'A table of video instance actors';
CREATE TABLE public.licenses (
    id integer NOT NULL,
    name text NOT NULL
);
COMMENT ON TABLE public.licenses IS 'A table of video licenses';
CREATE SEQUENCE public.licenses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.licenses_id_seq OWNED BY public.licenses.id;
CREATE TABLE public.users (
    id text NOT NULL,
    name text NOT NULL,
    "preferredUsername" text NOT NULL,
    url text NOT NULL,
    icon jsonb NOT NULL,
    summary text NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    "isHidden" boolean DEFAULT false NOT NULL
);
COMMENT ON TABLE public.users IS 'A table of video user actors';
CREATE TABLE public.videos (
    id text NOT NULL,
    published timestamp with time zone DEFAULT now() NOT NULL,
    updated timestamp with time zone,
    "originallyPublishedAt" timestamp with time zone,
    support text,
    sensitive boolean NOT NULL,
    "commentsEnabled" boolean NOT NULL,
    "isHidden" boolean DEFAULT false NOT NULL,
    content text,
    name text,
    url jsonb,
    icon jsonb,
    category_id integer,
    license_id integer,
    instance_id text NOT NULL,
    duration integer,
    views integer
);
COMMENT ON TABLE public.videos IS 'A table of all videos scraped';
ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);
ALTER TABLE ONLY public.licenses ALTER COLUMN id SET DEFAULT nextval('public.licenses_id_seq'::regclass);
ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_name_key UNIQUE (name);
ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.channels
    ADD CONSTRAINT channels_id_key UNIQUE (id);
ALTER TABLE ONLY public.channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.channels
    ADD CONSTRAINT channels_url_key UNIQUE (url);
ALTER TABLE ONLY public.instances
    ADD CONSTRAINT instances_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.licenses
    ADD CONSTRAINT licenses_name_key UNIQUE (name);
ALTER TABLE ONLY public.licenses
    ADD CONSTRAINT licenses_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_key UNIQUE (id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_url_key UNIQUE (url);
ALTER TABLE ONLY public.videos
    ADD CONSTRAINT videos_id_key UNIQUE (id);
ALTER TABLE ONLY public.videos
    ADD CONSTRAINT videos_pkey PRIMARY KEY (id);
CREATE INDEX categories_id_idx ON public.categories USING btree (id);
CREATE INDEX licenses_id_idx ON public.licenses USING btree (id);
CREATE INDEX videos_duration_idx ON public.videos USING btree (duration);
CREATE INDEX videos_id_idx ON public.videos USING btree (id);
CREATE TRIGGER set_public_actors_updated_at BEFORE UPDATE ON public.instances FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_actors_updated_at ON public.instances IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_channels_updated_at BEFORE UPDATE ON public.channels FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_channels_updated_at ON public.channels IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_users_updated_at BEFORE UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_users_updated_at ON public.users IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_videos_updated BEFORE UPDATE ON public.videos FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated();
COMMENT ON TRIGGER set_public_videos_updated ON public.videos IS 'trigger to set value of column "updated" to current timestamp on row update';
ALTER TABLE ONLY public.videos
    ADD CONSTRAINT videos_category_fkey FOREIGN KEY (category_id) REFERENCES public.categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.videos
    ADD CONSTRAINT videos_instance_id_fkey FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY public.videos
    ADD CONSTRAINT videos_license_fkey FOREIGN KEY (license_id) REFERENCES public.licenses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
