import { generateVideos, generateInstances } from "./generators"
import { makeClient, gql } from "../microservice/tasks/index"
import { chunkArray } from "../microservice/utils/fetch"
import { upsertInstance } from "../microservice/utils/dereferencers"

;(async () => {
  let videos = await generateVideos(50000)
  const instance_data = await upsertInstance(await generateInstances(1)[0])
  videos = videos.map(v => {
    return {
      ...v,
      instance: instance_data
    }
  })

  // bulk insert in store
  const bulk_insert_videos = gql`
    mutation BulkInsertVideos($videos: [videos_insert_input!]!) {
      insert_videos(
        objects: $videos
        on_conflict: {
          constraint: videos_object_id_key
          update_columns: [
            originallyPublishedAt
            support
            sensitive
            commentsEnabled
            content
            name
            url
            icon
            views
            user_id
            channel_id
            category_id
            license_id
            views
            language
          ]
        }
      ) {
        affected_rows
      }
    }
  `
  const chunkArray5000 = chunkArray(5000)
  await chunkArray5000(videos).forEach(async videos_chunk => {
    await makeClient().request(bulk_insert_videos, { videos: videos_chunk })
    console.log(videos_chunk.length, "videos added.")
  })
  console.log("TOTAL:", videos.length, "videos added.")
})()
