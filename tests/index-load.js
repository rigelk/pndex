"use strict"

const EasyGraphQLLoadTester = require("easygraphql-load-tester")
const fs = require("fs")
const path = require("path")

const schema = fs.readFileSync(path.join(__dirname, "schema.gql"), "utf8")

const args = {
  videos: {
    limit: 40
  }
}

const easyGraphQLLoadTester = new EasyGraphQLLoadTester([schema], args)

const queries = [
  {
    name: 'search_videos(args: {search: "Sport"}, limit: 40)',
    query: `
      {
        search_videos(args: {search: "Sport"}, limit: 40)
          object_id
          name
          content
          icon(path: ".url")
          url(path: "[0]")
        }
      }
    `
  }
]

const fullTextSearchCases = easyGraphQLLoadTester.artillery({
  customQueries: queries,
  selectedQueries: [
    "videos"
  ],
  withMutations: false
})

module.exports = {
  fullTextSearchCases
}
