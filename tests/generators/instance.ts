const faker = require('faker')
const { Schema } = require('faker-schema')

const instanceSchema = new Schema(() => ({
  object_id: `${faker.internet.url()}/accounts/peertube`,
  name: faker.company.companyName()
}))

export const generateInstances = n => instanceSchema.make(n)
