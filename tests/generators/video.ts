const faker = require('faker')
const { Schema } = require('faker-schema')

const videoSchema = new Schema(() => ({
  object_id: `${faker.internet.url()}/object/${faker.random.uuid()}`,
  name: faker.lorem.sentence(),
  content: faker.lorem.paragraphs(),
  duration: faker.random.number({
    min: 10, // seconds
    max: 2500 // seconds
  }),
  commentsEnabled: faker.random.boolean(),
  sensitive: faker.random.boolean(),
  views: faker.random.number(),
  category_id: faker.random.number({
    min: 1,
    max: 15
  }),
  license_id: faker.random.number({
    min: 1,
    max: 5
  }),
  language: faker.random.arrayElement(["de","fr","en"])
}))

export const generateVideos = n => videoSchema.make(n)
