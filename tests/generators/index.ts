import { generateVideos } from "./video"
import { generateInstances } from "./instance"

export {
  generateVideos,
  generateInstances
}