# pndex

A PeerTube indexer built on Hasura/PostgreSQL for the API,
ElasticSearch for the full text queries, and Express microservices
for the scraping logic.

## Quickstart

ElasticSearch needs more memory than regular services, so run
`sudo sysctl -w vm.max_map_count=262144` first.

Create your own `.env` from the `.env.example`, then use `docker-compose up -d`
to launch the whole service. The API will be bound to `localhost:8080/v1/graphql`.
Setup your [reverse proxy](https://docs.hasura.io/1.0/graphql/manual/deployment/enable-https.html#sample-configurations) and you should be ready for production.

## FAQ

### Is there a web user interface?

No, the service only has a backend an a GraphQL which you
can explore via introspection of its schema. Building a
fast and reliable API is the sole focus of this indexer.

### Does it crawl? Where are instances discovered?

There is no recursive crawling, as the service relies either on:
- manual input of an instance
- seed or sync with an instance list like https://instances.joinpeertube.org

Then, it updates its data by scanning instances (only if not 
scanned for more than 24 hours). There is no scanning retry
upon error.

### What makes it different?

Being a central point requires performance, and we focus on it by
decoupling the API from the controller. The API basically sits on
the Postgres database (see Hasura), while the scraper is a scalable
microservice.

The big advantage is that queries are directly answered by Hasura/
Postgres with no translation/serialization to an intermediate language.
Another advantage is that the API is directly generated from the
database, meaning complex filtering from the get-go. Finally, this
lets us scale the microservice without impacting the database 
bandwidth, and vice-versa.

### When/how scraping is done

__when__: scraping is done at 6am every day, using the clock of your system.
You can also make the microservice scrape at startup by setting
`INIT_SCRAPE` to `"true"`. Adding a *new* instance will also trigger
a scrape of its videos. In any case, you can use the CLI via
`npm run cli -- --help` to scrape instances yourself and directly
send the result to the API.

_The indexer has no actor, and will not subscribe to updates like a
regular PeerTube instance. The freshness of results is thus at most 24h._

__how__: scraping is done by looking at an instance's root actor's outbox,
and going through the collection looking for videos. You can insert
any actor as an instance (to support other implementations than
just PeerTube), but by default, giving a host to the CLI or 
microservice will only be possible for PeerTube for now.

_The scraper tries not to hammer instances, and throttles to 5 concurrent
requests per hostname._

### What User-Agent is used

`User-Agent: pndex/<version> (+https://framagit.org/rigelk/pndex)`

### What webhooks are called

Hasura uses triggers from the database to call webhooks that the
microservice is providing. Currently, the triggers are:

| model\trigger 	| insert          	| update          	| delete          	| manual          	|
|---------------	|-----------------	|-----------------	|-----------------	|-----------------	|
| instances     	| POST /instances 	|                  	|                 	| POST /instances 	|
| videos        	|                 	|                 	|                 	|                 	|
| users         	|                 	|                 	|                 	|                 	|
| channels      	|                 	|                 	|                 	|                 	|
