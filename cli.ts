import { config } from "dotenv"
import { scrapeInstance, barOptions, failsaifePromiseAll } from "./microservice/tasks/scrapeInstance"
import { syncInstanceStatusToInstancesList } from "./microservice/tasks/syncInstanceStatusToInstancesList"
import { api } from "./microservice/utils/fetch"
import normalize from "normalize-url"
const program = require("commander")
const _cliProgress = require('cli-progress')
const ora = require('ora')
const inquirer = require('inquirer')
inquirer.registerPrompt('search-checkbox', require('inquirer-search-checkbox'))

config()

program
  .name("pndex")
  .option(
    "-e, --endpoint",
    "override the HASURA_GRAPHQL_ENDPOINT env variable (see .env)"
  )
  .option(
    "-s, --secret",
    "override the HASURA_ADMIN_SECRET env variable (see .env)"
  )
  .option("-q, --quiet", "less visual output (like progress indicators)")
  .option("-d, --dry-run", "skip mutations")

program
  .command("scrape [url...]")
  .description("scrape the remote url(s) (must be an instance) for videos, or if empty known instances (by default INSTANCES_ENDPOINT='https://instances.joinpeertube.org/api/v1/instances' in .env)")
  .option(
    "--full",
    "ignore the stored last id (url) scraped in the instance and do a full scrape"
  )
  .option(
    "-i, --interactive",
    "prompts the user for the instances they want to scrape among those listed in the known instances of the configured directory"
  )
  .action(async (url, options) => {
    let multibar
    let spinner
    let instances = []

    if (process.env.PROGRESS !== "false") {
      spinner = ora(`Loading instances list form "${process.env.INSTANCES_ENDPOINT}"`)
      multibar = new _cliProgress.MultiBar(
        barOptions,
        _cliProgress.Presets.shades_grey
      )
    }
    if (url.length > 0) {
      instances = url.map(instance => normalize(instance, { forceHttps: true }))
    } else {
      spinner?.start()
      instances = (await api(process.env.INSTANCES_ENDPOINT) as any)?.data
      spinner?.succeed()

      if (options.interactive) {
        instances = await inquirer
          .prompt([
            {
              name: 'instances',
              message: 'Which instance(s) should be scraped?',
              type: 'search-checkbox',
              choices: instances.map(i => { return { name: i.host, value: i } }),
              pageSize: 10,
              validate: function( answer ) {
                if ( answer.length < 1 ) {
                  return "You must choose at least one instance."
                }
                return true;
              }
            }
          ])
          .then(answers => answers['instances'])
      }

      instances = instances?.map(instance => normalize(instance.host, { forceHttps: true }))
    }

    await failsaifePromiseAll(
      instances
        .map(instance_url => scrapeInstance(instance_url, options.full, multibar))
    )
    multibar?.stop()
  })
  .on("--help", function() {
    console.log("")
    console.log("Examples:")
    console.log("")
    console.log("  $ scrape https://framatube.org")
    console.log("  $ scrape https://framatube.org --full")
    console.log("  $ scrape https://framatube.org --interactive --full")
  })

program
  .command("sync")
  .description("to be done after a full scrape: removes instances not on the list anymore, and adds new ones")
  .action(_ => syncInstanceStatusToInstancesList())

program.parse(process.argv)

if (program.endpoint !== undefined)
  process.env.HASURA_GRAPHQL_ENDPOINT = program.endpoint
if (program.secret !== undefined)
  process.env.HASURA_ADMIN_SECRET = program.secret
if (program.quiet !== undefined) process.env.PROGRESS = "false"
if (program.dryRun !== undefined) process.env.SKIP_MUTATIONS = "true"
