# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. 

## Merge Request Process

1. Ensure any debug or comment is removed if unecessary to comprehend your code.
2. Ensure migrations are kept to a minimum and squashed.
3. Ensure your commit are squashed per functionality, and that their headline stays descriptive.
4. Document your changes in the MR description in more depth that the commit messages.
5. Performance improvements should be documented, and features inducing a performance tradeoff
   should be backed by numbers.

## Tool recommendations

Since we use Hasura, a good part of the workflow revolves around it and its tools. We include
a version of its cli in the development dependencies installed via `npm` *on purpose: to prevent
having an out-of-sync cli and ensure its has the same version as the docker image in use*. Setup
an alias to use it: `alias hasura='../node_modules/hasura-cli/hasura'`.

`cd` in the `hasura` directory to run `hasura` commands. A few useful ones:
- `hasura console --admin-secret <YOUR_SECRET>` (probably the first one you should do, to open the web interface to Hasura)
- `hasura migrate apply --admin-secret <YOUR_SECRET>` (applies the migrations to the database)
- `hasura migrate squash --from 1573407072545 --name <your_migration_name> --delete-source --admin-secret <YOUR_SECRET>` (an example of squash command, do not use it directly)
- `hasura migrate status --admin-secret <YOUR_SECRET>` (check which migrations are currently applied on the database)
