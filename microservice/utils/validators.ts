import * as express from "express"
import { Errors } from "typescript-rest"

export function eventTriggerValidator(req: express.Request): express.Request {
  if (!req.body.event?.op) {
    throw new Errors.BadRequestError("trigger payload not present or malformed")
  }
  return req
}
