import fetch from "node-fetch"
import { Headers } from "node-fetch"
const p = require("../../package.json")
const pRetry = require('p-retry')

export function api<T>(url: string, ap: boolean = true): Promise<T> {
  let headers = new Headers({
    'User-Agent': `${p.name}/${p.version} (+${p.repository})`
  })
  if (ap) headers.append('Accept', 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
  return fetch(url, { headers }).then(response => {
    if (!response.ok) {
      throw new pRetry.AbortError(response)
    }
    return response.json() as Promise<T>
  })
}

/**
* Videos are sent in bulk, and large instances have thousands of them.
* Chunks prevent going to hard on the API at a given time, which is
* especially useful when the API is already under load by clients'
* requests, but not required.
*/
export const chunkArray = chunkSize => array => {
 return array.reduce((acc, each, index, src) => {
     if (!(index % chunkSize)) { 
         return [...acc, src.slice(index, index + chunkSize)]
     } 
     return acc;
     },
 [])
}
