import { Video } from "../types"
import { pick } from "lodash"
import { api } from "./fetch"
import { has } from "lodash"
const pRetry = require('p-retry')

export const upsertUser = async (video: Video, instance) => {
  const user = await dereferenceUser(video, instance)
  return user
    ? {
        data: user,
        on_conflict: {constraint: 'users_object_id_key', update_columns: ['summary']}
      }
    : null
}

export const dereferenceUser = async (video: Video, instance) => {
  if (!has(video, 'attributedTo')) return null
  let user = await pRetry(() => api((video.attributedTo as Array<any>).find(a => a.type === "Person")?.id as string, true), { retries: 1 }).catch(error => null)
  if (user) user.object_id = user.id
  user.instance = {
    data: instance,
    on_conflict: {constraint: 'instances_object_id_key', update_columns: ['name']}
  }
  return user
    ? pick(user,
        'object_id',
        'name',
        'preferredUsername',
        'url',
        'icon',
        'summary',
        'instance'
      )
    : null
}

export const upsertChannel = async (video: Video, instance, upsertUser = null) => {
  const channel = await dereferenceChannel(video, instance, upsertUser)
  return channel
    ? {
        data: channel,
        on_conflict: {constraint: 'channels_object_id_key', update_columns: ['summary', 'support', 'icon', 'name']}
      }
    : null
}

export const dereferenceChannel = async (video: Video, instance, upsertUser = null) => {
  if (!video.attributedTo) return null
  const channel = await pRetry(() => api((video.attributedTo as Array<any>).find(a => a.type === "Group")?.id as string, true), { retries: 1 }).catch(error => null)
  if (channel) {
    channel.object_id = channel.id
    channel.user = upsertUser ? upsertUser : {
      data: await dereferenceUser(video, instance),
      on_conflict: {constraint: 'users_object_id_key', update_columns: ['summary', 'icon', 'name']}
    }
  }
  return channel
    ? pick(channel,
        'object_id',
        'name',
        'url',
        'icon',
        'summary',
        'user'
      )
    : null
}

export const upsertInstance = async (instance) => {
  return {
    data: instance,
    on_conflict: {constraint: 'instances_object_id_key', update_columns: ['nodeinfo', 'last_activity_fetched', 'name']}
  }
}

export const dereferenceInstance = async (instance, last_activity_fetched = null) => {
  /**
   * @param instance is in the form "https://example.com"
   */
  const instance_nodeinfo = await api(`${instance}/nodeinfo/2.0.json`, false).catch(error => null)
  return {
    object_id: `${instance}/accounts/peertube`,
    nodeinfo: instance_nodeinfo,
    name: (instance_nodeinfo as any)?.metadata.nodeName,
    last_activity_fetched
  }
}
