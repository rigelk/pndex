import { PassportAuthenticator } from "typescript-rest"
import { Strategy } from "passport-http-bearer"

const TOKEN: string = process.env.TOKEN

const strategy = new Strategy(
  (token: string, done: (a: null, b: any) => void) => {
    if (token === TOKEN) {
      const user: any = {
        roles: ["admin"],
        username: "admin"
      }
      done(null, user)
    }
    done(null, false)
  }
)

export const fixedTokenStrategy = new PassportAuthenticator(strategy, {
  deserializeUser: (user: any) => JSON.parse(user),
  serializeUser: (user: any) => JSON.stringify(user)
})
