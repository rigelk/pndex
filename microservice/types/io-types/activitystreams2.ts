import * as t from "io-ts"

const ASObject = t.partial({
  id: t.string,
  content: t.string,
  url: t.string,
  name: t.string,
  summary: t.string
})
export type ASObject = t.TypeOf<typeof ASObject>

const OrderedCollection = t.partial({
  orderedItems: t.array(ASObject),
  totalItems: t.number
})
export type OrderedCollection = t.TypeOf<typeof OrderedCollection>

const Video = t.partial({
  id: t.string,
  content: t.string,
  url: t.string,
  name: t.string,
  duration: t.string,
  commentsEnabled: t.boolean,
  sensitive: t.boolean,
  published: t.string,
  updated: t.string,
  support: t.string,
  originallyPublishedAt: t.string,
  icon: t.string
})
export type Video = t.TypeOf<typeof Video>
