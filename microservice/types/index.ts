import { ASObject } from 'activitystreams2'

// this is not an exhaustive definition, for that look
// at the hasura type
export declare class Instance {
  id?: string
  last_activity_fetched?: string
}

export declare class OrderedCollection<T> extends ASObject {
  orderedItems?: T[];
  totalItems?: number;
}

export declare class OrderedCollectionPage<T> extends OrderedCollection<T> {
  first?: string;
  next?: string;
}

interface Trigger {
  name: string
}

interface Table {
  schema: string
  name: string
}

interface EventData<T> {
  old?: T
  new?: T
}

interface Event<T> {
  session_variables: any
  op: "INSERT" | "UPDATE" | "DELETE" | "MANUAL"
  data: EventData<T>
}

export declare class EventTriggerPayload<T> {
  id: string
  created_at: Date
  trigger: Trigger
  table: Table
  event: Event<T>
}

interface Enum {
  identifier: Number,
  name: String
}

interface LangEnum {
  identifier: String,
  name: String
}

export declare class Video extends ASObject {
  category?: Enum
  license?: Enum
  language?: LangEnum
  duration?: String

  // fields introduced by Hasura to simplify relations
  user?: any
  user_id?: string
  channel?: any
  channel_id?: string
}
