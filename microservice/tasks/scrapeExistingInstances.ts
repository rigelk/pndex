import { queue, scrapeInstance, makeClient, gql } from './index'

export const scrapeExistingInstances = async () => {
  // STEP 1: collect all instances that were not updated in the last 24h
  const instances_to_sync_with_query = gql`
    query InstancesToSyncWith($yesterday: timestamptz) {
      instances(where: {_and: {isCollected: {_eq: true}, updated_at: {_lte: $yesterday}}}) {
        object_id
      }
    }
  `
  const yesterday = ( d => new Date(d.setDate(d.getDate()-1)) )(new Date)
  makeClient()
    .request(instances_to_sync_with_query, { yesterday })
    .then(res => {
      // STEP 2: scrape these instances
      res?.data?.instances?.map(i => i.object_id).map(i => queue.add(() => scrapeInstance(i)))
    })
}
