import { GraphQLClient } from 'graphql-request'
import { scrapeInstance } from "./scrapeInstance"
import { scrapeExistingInstances } from "./scrapeExistingInstances"
import { syncInstanceStatusToInstancesList } from "./syncInstanceStatusToInstancesList"
const p = require("../../package.json")
const {default: PQueue} = require('p-queue')

let client = undefined
const makeClient = () => {
  if (client) return client
  client = new GraphQLClient(process.env.HASURA_GRAPHQL_ENDPOINT, {
    headers: {
      'User-Agent': `${p.name}/${p.version} (+${p.repository})`,
      // authorization: `JWT ${HASURA_JWT}`
      'x-hasura-admin-secret': process.env.HASURA_GRAPHQL_ADMIN_SECRET
    }
  })
  return client
}

function gql(literals: TemplateStringsArray, ...values: any[]) {
  let output = ''
  let index
  for (index = 0; index < values.length; index++) {
    output += literals[index] + values[index]
  }

  output += literals[index]
  return output
}

const queue = new PQueue({concurrency: 10})

export {
  queue,
  makeClient,
  gql,
  scrapeInstance,
  scrapeExistingInstances,
  syncInstanceStatusToInstancesList
}
