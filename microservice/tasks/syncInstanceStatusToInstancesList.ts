import { queue, makeClient, gql } from './index'
import { api } from "../utils/fetch"
import normalize from "normalize-url"
import { scrapeInstance } from "./scrapeInstance"
import { without } from "lodash"

export const syncInstanceStatusToInstancesList = async () => {
  // STEP 1: collect all instances in db
  const instances_to_sync_with_query = gql`
    query InstancesToSyncWith {
      instances(where: {isHidden: {_neq: true}}) {
        object_id
      }
    }
  `
  const dbInstances = await makeClient().request(instances_to_sync_with_query).then(d => d.instances.map(i => i.object_id))

  // STEP 2: collect all instances on the instances list
  const listInstances = (await api(process.env.INSTANCES_ENDPOINT) as any)?.data.map(d => `${normalize(d.host, { forceHttps: true })}/accounts/peertube`)

  // STEP 3: intersections of the lists
  // see https://lodash.com/docs/2.4.2#without
  const newInstances = without(listInstances, ...dbInstances)
  const mutedOrRemovedInstances = without(dbInstances, ...listInstances)
  console.log(newInstances, mutedOrRemovedInstances)

  // STEP 4: delete muted or removed instances from the instances list
  const delete_instances_mutation = gql`
    mutation($mutedOrRemovedInstances: [String!]) {
      delete_instances(where: {object_id: {_in: $mutedOrRemovedInstances}}) {
        affected_rows
      }
    }
  `
  makeClient().request(delete_instances_mutation, { mutedOrRemovedInstances })

  // STEP 5: add new instances
  newInstances?.map(i => queue.add(() => scrapeInstance(i)))
}
