import { OrderedCollectionPage, Video } from "../types"
import { Activity } from "activitystreams2"
import { pick, omit, truncate } from "lodash"
import normalize from "normalize-url"
import { isRight } from 'fp-ts/lib/Either'
import * as t from "io-ts"
import { api, chunkArray } from "../utils/fetch"
import { makeClient, gql } from "./index"
import { upsertUser, upsertChannel, dereferenceInstance, upsertInstance } from "../utils/dereferencers"
const pRetry = require('p-retry')
const {default: PQueue} = require('p-queue')
const _cliProgress = require('cli-progress')
const _colors = require('colors')

export function failsaifePromiseAll (promises) {
  return Promise.all(
    promises.map(p => p.catch(error => null))
  )
}

function barFormatter (options, params, payload) {
  // bar grows dynamically by current progress - no whitespace is added
  const bar = options.barCompleteString.substr(0, Math.round(params.progress*options.barsize));
  const counter = (unknownTotal = false) => truncate((`${params.value}/${unknownTotal ? '?' : params.total}`).padEnd(11), { length: 11 })

  // end value reached ?
  // change color to green once finished and pad fields
  const normalized_instance = truncate(normalize(payload.instance, {stripProtocol: true}).padEnd(25), { length: 25 })
  if (payload.error) {
    return '# ' + normalized_instance + '   ' + _colors.red(counter(true)) + ' |' + bar + '| ' + payload.status
  } else if (params.value >= params.total) {
    return '# ' + _colors.grey(normalized_instance) + '   ' + _colors.green(counter()) + ' |' + bar + '| ' + payload.status
  } else {
    return '# ' + normalized_instance + '   ' + _colors.yellow(counter()) + ' |' + bar + '| ' + payload.status
  }
}

export const barOptions = {
  format: barFormatter,
  barCompleteChar: '\u2588',
  barIncompleteChar: '\u2591',
  hideCursor: true,
  stopOnComplete: false
}

export const scrapeInstance = async (instance: string, full: boolean = false, multibar = null) => {
  /**
   * Scrapes a given instance, upserts the instance and its videos
   * @param full forces a full scrape, independently of the instance's last fetched activity
   * TODO: ensure indempotency (should be ready to handle cases when called more than once with the same parameters)
   */
  const queue = new PQueue({ concurrency: 5 })
  const actorUrl = `${instance}/accounts/peertube`
  let videos: Video[] = []
  const instance_last_activity_fetched_query = gql`
    query InstanceLastFetchedActivity($object_id: String!) {
      instances(where: {object_id: {_eq: $object_id}}) {
        last_activity_fetched
      }
    }
  `

  try {
    let error = null
    // instance object creation
    let sent_instance = await dereferenceInstance(instance)
    // collection retrieval
    let orderedCollection = await pRetry(() => api<OrderedCollectionPage<Activity>>(`${actorUrl}/outbox`), { retries: 3 }).catch(e => {
      error = e.code
      return e
    })

    let bar: any = null
    if (process.env.PROGRESS !== "false") {
      bar = multibar ? multibar.create(orderedCollection.totalItems, 0, {}) : new _cliProgress.SingleBar(barOptions)
    }
    bar?.start(orderedCollection?.totalItems, 0, { instance, status: error || '', error })
    if (error) {
      bar?.stop()
      return // STOP EXECUTION
    }

    let next: string | null = orderedCollection?.first
    let until: string | null = (await makeClient().request(instance_last_activity_fetched_query, { object_id: actorUrl }).catch(e => null))?.instances[0]?.last_activity_fetched
    let cont = true

    // going through pages to gather activities from most recent posted activity to least recent, and
    // potentially stopping mid-way under some condition.
    while(next && cont) {
      const page: OrderedCollectionPage<Activity> = await api<OrderedCollectionPage<Activity>>(next)

      for (const activity of page?.orderedItems) {
        // check if video already known
        if (!full && activity.object === until) {
          cont = false
          bar?.update(null, { status: 'stopping on last fetched activity' })
          break
        }
        // the first activity becomes the last_activity_fetched
        if (!sent_instance.last_activity_fetched) {
          sent_instance.last_activity_fetched = activity.object // url of the last fetched object, that will make the next scrape stop
        }
        // add to the queue a task to dereference the video IRI from the activity
        (async () => {
          await queue.add(async () => {
            let video: Video = await pRetry(() => api(activity.object as string, true), { retries: 1 }).catch(error => null)
            if (video) {
              video.user = await upsertUser(video, sent_instance),
              video.channel = await upsertChannel(video, sent_instance, video.user)
              video = pick(video,
                'id',
                'url',
                'content',
                'name',
                'duration',
                'commentsEnabled',
                'sensitive',
                'published',
                'updated',
                'support',
                'originallyPublishedAt',
                'icon',
                'views',
                'user',
                'channel',
                // these are meant to be modified afterwards
                'category',
                'license',
                'language'
              )
              videos.push(video)
            }
          })
          bar?.increment() // once the task is done, increment the progress bar
        })()
      }

      next = page.next
    }

    // wait for all tasks to complete
    await queue.onIdle()
    videos = videos.filter(v => v) // remove null activities (URLs that yield errors)

    if (videos?.length > 0) {
      // prepare the mutation to add the videos
      const upsert_sent_instance = await upsertInstance(sent_instance)
      const sent_videos = videos.map(v => {
        return omit({
          ...v,
          object_id: v.id,
          category_id: v.category?.identifier,
          license_id: v.license?.identifier,
          language: isRight(t.string.decode(v.language?.identifier)) ? v.language?.identifier : null,
          duration: +v.duration?.substring(2).slice(0, -1),
          instance: upsert_sent_instance
        }, [
          'id',
          'category',
          'license'
        ])
      })
      .filter(v => v.object_id)

      // bulk insert in store
      const bulk_insert_videos = gql`
        mutation BulkInsertVideos($videos: [videos_insert_input!]!) {
          insert_videos(objects: $videos, on_conflict: {constraint: videos_object_id_key, update_columns: [originallyPublishedAt, support, sensitive, commentsEnabled, content, name, url, icon, views, user_id, channel_id, category_id, license_id, views, language]}) {
            affected_rows
          }
        }
      `
      if (process.env.SKIP_MUTATIONS !== "true") {
        const chunkArray50 = chunkArray(50)
        chunkArray50(sent_videos).reverse().forEach(async videos_chunk => {
          await makeClient().request(bulk_insert_videos, { videos: videos_chunk })
          if (process.env.PROGRESS === "false") console.log(`${videos_chunk.length} videos scraped from ${sent_instance.object_id}`)
        })
      }
    }

    bar?.stop()
  } catch (err) {
    /**
     * Errors (especially network errors) should be supported and dealt with internally
     * as they are usually the sign that an element needs to be removed from the list
     * of sent activities.
     */
    console.log("Unsupported Error /!\\", err)
  }
}
