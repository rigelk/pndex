import * as express from "express"
import { config } from "dotenv"
import {
  Accept,
  Server,
  Path,
  POST,
  Errors,
  PreProcessor
} from "typescript-rest"
import { queue, scrapeInstance } from "./microservice/tasks"
import { EventTriggerPayload, Instance } from "./microservice/types"
import { eventTriggerValidator } from "./microservice/utils/validators"
import { scrapeExistingInstances, syncInstanceStatusToInstancesList } from "./microservice/tasks"

const cron = require('cron').CronJob

config()
process.env.PROGRESS = "false"


@Path("instances")
@Accept("application/json")
class InstanceService {
  /**
   * Webhooks are only done via POST
   */
  @POST
  @PreProcessor(eventTriggerValidator)
  trigger(payload: any): void {
    switch (payload.event.op) {
      case "INSERT":
        queue.add(scrapeInstance(payload.event.data?.new?.id))
        break

      case "MANUAL":
        switch (payload.trigger?.name) {
          case "instance_scrape":
            queue.add(scrapeInstance(payload.event.data?.new?.id, true))
            break

          default:
            throw new Errors.NotImplementedError(
              `The MANUAL operation of the ${payload.trigger?.name} trigger on "instances" is not yet supported`
            )
        }
        break

      default:
        throw new Errors.NotImplementedError(
          `The ${payload.event.op} operation on "instances" is not yet supported`
        )
    }
    // method returns void, an empty body is sent with a 204 status code.
  }
}

let app: express.Application = express()
Server.buildServices(app)
app.listen(+process.env.PORT || 8081, function() {
  console.log(
    `server listening on "http://localhost:${+process.env.PORT || 8081}"`
  )

  // scrape every night at 6am.
  new cron('0 6 * * *', async () => {
    await syncInstanceStatusToInstancesList()
    await scrapeExistingInstances()
  }, null, null, null, null, process.env.INIT_SCRAPE === "true").start() // null values are skipped parameter definitons of the constructor, true is to say "I want the job to run once at startup too"
})
